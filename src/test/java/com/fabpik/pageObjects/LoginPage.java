package com.fabpik.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	
	WebDriver ldriver;
	
	public LoginPage(WebDriver rdriver)
	{
		ldriver=rdriver;
		PageFactory.initElements(rdriver, this);
	}
	
	@FindBy(xpath ="//label[contains(text(),'Login')]")
	@CacheLookup
	WebElement homeLogin;
	
	@FindBy(xpath="//div[@class='controls']//input[@type='text']")
	@CacheLookup
	WebElement emailIDtxt;
	
	@FindBy(xpath="//button[@type='submit']")
	@CacheLookup
	WebElement pinkLogin;
	
	@FindBy(xpath="//button[@class='btn btn-style-bg mb-0 text-uppercase m-b30']")
	@CacheLookup
	WebElement loginwithpwd;
	
	
	@FindBy(xpath="//input[@type='password']")
	@CacheLookup
	WebElement pwdtxt;
	
	@FindBy(xpath="//button[@type='submit']")
	@CacheLookup
	WebElement loginfinal;
	
	@FindBy(xpath="//label[contains(text(),'Log Out')]")
	@CacheLookup
	WebElement custlogout;
	
	
	//Action Methods
	
	public void clickhomelogin()
	{
		homeLogin.click();
	}
	public void enteremail(String email)
	{
	emailIDtxt.sendKeys(email);
	}
	
	public void clickpinklogin()
	{
		pinkLogin.click();
	}
	public void clickloginwithpwd()
	{
		loginwithpwd.click();
	}
	public void enterpwd(String pwd)
	{
		pwdtxt.sendKeys(pwd);
	}
	
	public void clickloginFinal()
	{
		loginfinal.click();
	}
	public void clicklogout()
	{
		custlogout.click();
	}
	
	
}
