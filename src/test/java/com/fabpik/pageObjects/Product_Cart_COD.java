package com.fabpik.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Product_Cart_COD {
WebDriver ldriver;
	
	public Product_Cart_COD(WebDriver rdriver)
	{
		ldriver=rdriver;
		PageFactory.initElements(rdriver, this);
	}
	// Item search & Add cart test
//	@FindBy(xpath ="//span[@class='menu-text main-nav-title'][contains(text(),'Footwear')]")
//	@CacheLookup
//	WebElement clickmenuitem;

	// PRODUCT ORDERFROM CATEGORY FOOTWARE(BY TYPE).
	
	@FindBy(xpath ="//div[@class='product-search-widget']//input[@type='text']")
	@CacheLookup
	WebElement itemsearchbox;
	
//	@FindBy(xpath ="//span[contains(text(),'Footwear > By Type > By Type Leather')]")
//	@CacheLookup
//	WebElement clicksrchditem;
	
	@FindBy(xpath ="//span[contains(text(),'Leather Winter sandals')]")
	@CacheLookup
	WebElement clicksrchdProduct;
	
	
	@FindBy(xpath ="//input[@id='Peach_0_']")
	@CacheLookup
	WebElement clickcolor;
	
	@FindBy(xpath ="//span[contains(text(),'18-24 M')]")
	@CacheLookup
	WebElement clicksize;
	
	@FindBy(xpath ="//button[contains(text(),'Add To Cart')]")
	@CacheLookup
	WebElement clickaddtocart;
		
	@FindBy(xpath ="//button[contains(text(),'Go To Cart')]")
	@CacheLookup
	WebElement clickGotocart;
	
	@FindBy(xpath ="//button[contains(text(),'Place To Order')]")
	@CacheLookup
	WebElement clickPlaceToOrder;
	
	@FindBy(xpath ="//div[@id='viewdetails']//button[@type='submit'][contains(text(),'MAKE PAYMENT')]")
	@CacheLookup
	WebElement clickMakePayment;
	
	@FindBy(xpath ="//div[@class='col-lg-1 col-xs-1 col-sm-1 p-a0']//div[@class='form-radio d-flex']//div[@class='mt-1 radio']//label")
	@CacheLookup   
	WebElement clickCOD;
	
	@FindBy(xpath ="//div[@id='viewdetails']//button[@type='submit']//span[@class='text-white ng-star-inserted'][contains(text(),'Place Order')]")
	@CacheLookup
	WebElement clickPlaceOrder;
	
	//VERIFY THE ORDER IS CONFIRMED AND PRINT THE ORDER NUMBER.
	
//NOW ADD OBJECTS TO IT.

	public void itemsearchtxtbox(String Leather)
	{
		itemsearchbox.sendKeys("Leather winter Sandals");
	}
	
//	public void clickonnavigation()
//	{
//		clicksrchditem.click();
//	}
	
	public void clickonproduct()
	{
		clicksrchdProduct.click();
	}
	
	// NEW TAB WILL BE OPEN- APPLY WINDOW HANDLER 
	
	public void selectcolor()
	{
		clickcolor.click();
	}
	
	public void selectsize()
	{
		clicksize.click();
	}
	
	public void clickAddtocartbtn()
	{
		clickaddtocart.click();
	}
	public void clickGotocartbtn()
	{
		clickGotocart.click();
	}
	
	public void Placetoorderbtn()
	{
		clickPlaceToOrder.click();
	}
	
	public void makepaymentbtn()
	{
		clickMakePayment.click();
	}
	
	public void CODradiobtn()
	{
		clickCOD.click();
	}
	
	public void placeorderbtn()
	{
		clickPlaceOrder.click();
	}
	
	
	
	
	
	
}