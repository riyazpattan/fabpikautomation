package com.fabpik.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OrdersActivityPOM {
WebDriver ldriver;
	
	public OrdersActivityPOM(WebDriver rdriver)
	{
		ldriver=rdriver;
		PageFactory.initElements(rdriver, this);
	}
	

	// ORDERS ACTIVITIES / STATUS.
	
	@FindBy(xpath ="//label[contains(text(),'Profile')]")
	@CacheLookup
	WebElement clickprofile;
	
	@FindBy(xpath ="//span[contains(text(),'Orders')]")
	@CacheLookup
	WebElement orderslabel ;
	
	@FindBy(xpath ="//span[contains(text(),'10169')]")
	@CacheLookup
	WebElement firstorder ;
	
	@FindBy(xpath ="//span[contains(text(),'Orders')]")
	@CacheLookup
	WebElement orderslabel2 ;
	
	@FindBy(xpath ="//div[contains(text(),'Returned')]")
	@CacheLookup
	WebElement clickReturn ;
	
	@FindBy(xpath ="//div[contains(text(),'Order in Process')]")
	@CacheLookup
	WebElement clickOrderinprocess ;
	
	@FindBy(xpath ="//div[contains(text(),'Delivered')]")
	@CacheLookup
	WebElement clickDelivered;
	
	@FindBy(xpath ="//div[contains(text(),'Cancelled')]")
	@CacheLookup
	WebElement clickCancelled;
	
		
	
	//ACTION METHODS.
	
	public void clickonONprofile()
	{
		clickprofile.click();
	}
	
	public void clickonOrdersLabel()
	{
		orderslabel.click();
	}
	public void clickonFirstOrder()
	{
		firstorder.click();
	}
	
	// Here check the order details page is viewd.
	
	// now click Order Label again.
	
	public void clickagainOrderslabel()
	{
		orderslabel2.click();
	}
	
	public void clickonReturn()
	{
		clickReturn.click();
	}
	public void clickinprocess()
	{
		clickOrderinprocess.click();
	}
	
	public void clickonDelivered()
	{
		clickDelivered.click();
	}
	
	public void clickonCancelled()
	{
		clickCancelled.click();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
