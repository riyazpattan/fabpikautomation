package com.fabpik.testCases;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.fabpik.pageObjects.LoginPage;
import com.fabpik.pageObjects.Product_Cart_COD;

import junit.framework.Assert;

public class TC_Login_PlaceOrder extends BaseClass   {
	
	
@Test
public void login_placeorder() throws IOException, InterruptedException
{
	
	// PRODUCT ORDERFROM CATEGORY FOOTWARE(BY TYPE).    CHANGE THE ELEMENT FROM SEARCH	 
//	  Thread.sleep(1000);
	logger.info("URL is Opened");
	driver.manage().window().maximize();
	Thread.sleep(1500);
	//User Login
		LoginPage lp=new LoginPage(driver);
		lp.clickhomelogin();
		logger.info("Clicked on LOGIN button in Home page");
		Thread.sleep(1000);
		lp.enteremail(username);
		lp.clickpinklogin();
		logger.info("Clicked after email Login button");
		Thread.sleep(2500);
		lp.clickloginwithpwd();
		logger.info("Clicking on LOGINWITHPASSWORD Button");
		Thread.sleep(2500);
		lp.enterpwd(password);
		logger.info("Password entered");
		Thread.sleep(2500);
		lp.clickloginFinal();
		logger.info("Clicked on FINAL LOGIN Button");
		Thread.sleep(2500);
		
	//SEARCH PRODUCT - ADD TO CART - CHECKOUT - CONFIRM ORDER.
	
		Product_Cart_COD pcc= new Product_Cart_COD(driver);
		
		pcc.itemsearchtxtbox(" Leather winter Sandals");
		Thread.sleep(1500);
//		pcc.clickonnavigation();
		logger.info("clicked on search navigation" );
		Thread.sleep(1500);
		pcc.clickonproduct();
		logger.info("Selected a Product");
		Thread.sleep(1500);
	
		 String MainWindow=driver.getWindowHandle();		
	 Thread.sleep(1500);
	        // To handle all new opened window.				
	            Set<String> s1=driver.getWindowHandles();		
	        Iterator<String> i1=s1.iterator();		
	        		
	        while(i1.hasNext())			
	        {		
	            String ChildWindow=i1.next();		
	            Thread.sleep(1500);    		
	            if(!MainWindow.equalsIgnoreCase(ChildWindow))			
	            {    		
	            	Thread.sleep(1500);
	                    // Switching to Child window
	                    driver.switchTo().window(ChildWindow);	
		 logger.info("Switching New Window"); 
	                  
	                        
	Thread.sleep(2500);
		pcc.selectcolor();
		logger.info("Selected COLOR");
		Thread.sleep(1500);
		
		pcc.selectsize();
		logger.info("Selected COLOR");
		Thread.sleep(2500);
		pcc.clickAddtocartbtn();
		logger.info("Added to cart");
		Thread.sleep(1500);
		pcc.clickGotocartbtn();
		Thread.sleep(2500);
		pcc.Placetoorderbtn();
		logger.info("Place to Order button clicked");
		pcc.makepaymentbtn();
		Thread.sleep(2500);
		pcc.CODradiobtn();
		logger.info("Selected COD");
		Thread.sleep(3500);
		pcc.placeorderbtn();
		logger.info("PLACED ORDER");
		Thread.sleep(2500);
	
		boolean res=driver.getPageSource().contains("Order Confirmed");
		if (res==true)
		{
			Assert.assertTrue(true);
		}
		else
		{
			captureScreen(driver, "Checkout");
			Assert.assertTrue(false);
		}
		

	}

	        }}}
