package com.fabpik.testCases;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.fabpik.pageObjects.LoginPage;
import com.fabpik.pageObjects.OrdersActivityPOM;
import junit.framework.Assert;

public class TC_Cust_Orders_003 extends BaseClass {
	@Test
	public void login() throws IOException, InterruptedException
	{
		
	// need to modify the element for latest order in orders list.	
		
		// Orders tracking.	 
//		  Thread.sleep(1000);
		logger.info("URL is Opened");
		driver.manage().window().maximize();
		//Thread.sleep(1000);
		//User Login
			LoginPage lp=new LoginPage(driver);
			lp.clickhomelogin();
			logger.info("Clicked on LOGIN button in Home page");
			Thread.sleep(1500);
			lp.enteremail(username);
			lp.clickpinklogin();
			logger.info("Clicked after email Login button");
			Thread.sleep(2500);
			lp.clickloginwithpwd();
			logger.info("Clicking on LOGINWITHPASSWORD Button");
			Thread.sleep(2500);
			lp.enterpwd(password);
			logger.info("Password entered");
			Thread.sleep(2500);
			lp.clickloginFinal();
			logger.info("Clicked on FINAL LOGIN Button");
			Thread.sleep(2500);
			
		//SEARCH PRODUCT - ADD TO CART - CHECKOUT - CONFIRM ORDER.
		
			OrdersActivityPOM ord= new OrdersActivityPOM(driver);
							ord.clickonONprofile();
							Thread.sleep(1500);
							logger.info("Clicked on Profile button");
							ord.clickonOrdersLabel();
							Thread.sleep(1500);
							logger.info("Clicked on Order label");
							
							boolean res=driver.getPageSource().contains("My Orders");
							Thread.sleep(1000);	
							System.out.println("The page title is : " + res );
							
							if (res==true)
							{
								Assert.assertTrue(true);
							}
							else
							{
								captureScreen(driver, "My Orders");
								Assert.assertTrue(false);
							}
							ord.clickonFirstOrder();
							Thread.sleep(2500);
							logger.info("Clicked on first Order in MY Orders");
							
//							boolean res1=driver.getPageSource().contains("Order Details");
//						System.out.println("PRINTING ORDER DETAILS VIEW is:  xxx" + res1 );
//						Thread.sleep(1500);
//							if (res1==true)
//							{
//								Assert.assertTrue(true);
//							}
//							else
//							{
//								captureScreen(driver, "My Orders");
//								Assert.assertTrue(false);
//							}
//							Thread.sleep(1500);
							ord.clickagainOrderslabel();
							Thread.sleep(1500);
							
							ord.clickonReturn();
							Thread.sleep(1500);
							ord.clickinprocess();
							Thread.sleep(1500);
							ord.clickonDelivered();
							Thread.sleep(1500);
							ord.clickonCancelled();
							Thread.sleep(1500);
							logger.info("Clicked on CANCELLED SUCCESSFULLY");
							logger.info("TEST CASE 003 PASSED SUCCESSFULLY");
		
}

	
}