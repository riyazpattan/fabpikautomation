package com.fabpik.testCases;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.fabpik.utilities.ReadConfig;

public class BaseClass {

	ReadConfig readconfig=new ReadConfig();
	
	
	public String baseURL=readconfig.getApplicationURL();
	public String username=readconfig.getUsername();
	public String password=readconfig.getpassword();
	public static WebDriver driver;
	public static Logger logger;
	
	@Parameters("browser")
	@BeforeClass
	public void setup(String br)
	{
	logger =Logger.getLogger("FABPIK ECOM");
	PropertyConfigurator.configure("Log4j.properties");
	if(br.equals("chrome"))
	{
	System.setProperty("webdriver.chrome.driver", readconfig.getchromepath());
	driver=new ChromeDriver();
	System.out.println("OPENED CHROME");
	logger.info("OPENED CHROME");
	}else if(br.equals("firefox"))
	{
			System.setProperty("webdriver.gecko.driver", readconfig.getfirefoxpath());
			driver=new FirefoxDriver();	
			System.out.println("OPENED FIREFOX");
			logger.info("OPENED FIREFOX");
	} else if(br.equals("ie"))
	{
		System.setProperty("webdriver.ie.driver", readconfig.getiepath());
		driver=new InternetExplorerDriver();
		System.out.println("OPENED Internet Exploerer");
		logger.info("OPENED Internet Explorer");
	}
	driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	driver.get(baseURL);
	
	}
	
	
	@AfterClass
	public void tearDown()
	{
		driver.quit();
	}

public void captureScreen(WebDriver driver, String tname) throws IOException {
	TakesScreenshot ts = (TakesScreenshot) driver;
	File source = ts.getScreenshotAs(OutputType.FILE);
	File target = new File(System.getProperty("user.dir")+"/Screenshots/"+ tname+".png");
	FileUtils.copyFile(source,  target);
	System.out.println("Screenshot taken");
}}
//	}
	/*
	
	@BeforeClass
	public void setup(String br)
	{
		//System.setProperty("Webdriver.firefox.driver","System.getProperty("user.dir")+"//Drivers//geckodriver.exe");
		//System.setProperty("Webdriver.firefox.driver","C://Drivers//geckodriver.exe");
		
		//System.setProperty("webdriver.gecko.driver", "C://Drivers//geckodriver.exe");
		if(br.equals("firefox"))
		{
		System.setProperty("webdriver.gecko.driver",readconfig.getFirefoxPath());
		 driver = new FirefoxDriver();
		}
		else if(br.equals("chrome"))
		{
			System.setProperty("webdriver.chrome.driver",readconfig.getChromePath());
			 driver = new ChromeDriver();
		}
		else if(br.equals("ie"))
		{
			System.setProperty("webdriver.ie.driver",readconfig.getIEPath());
			 driver = new InternetExplorerDriver();
		}
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.get(baseURL);
		 logger = Logger.getLogger("inetBankinglog");
			PropertyConfigurator.configure("Log4j.properties");
	}
	@AfterClass
	public void tearDown()
	{
	driver.quit(); 
	}
	
	public void captureScreen(WebDriver driver, String tname) throws IOException {
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		File target = new File(System.getProperty("user.dir")+"/Screenshots/"+ tname+".png");
		FileUtils.copyFile(source,  target);
		System.out.println("Screenshot taken");
		
	}
	public String randomestring()
	{
		String generatedstring=RandomStringUtils.randomAlphabetic(5);
		return(generatedstring);
	}
public static String randomeNum() {
	String generatedString2 = RandomStringUtils.randomNumeric(5);
	return (generatedString2);
	
	*/

	
	
