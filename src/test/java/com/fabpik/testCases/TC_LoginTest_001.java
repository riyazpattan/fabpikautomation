package com.fabpik.testCases;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fabpik.pageObjects.LoginPage;

public class TC_LoginTest_001 extends BaseClass {

	
	

	@Test
	public void loginTest() throws InterruptedException, IOException
	{
	
	//	Thread.sleep(1000);
		logger.info("URL is Opened");
		driver.manage().window().maximize();
		logger.info("THE PAGE TITLE IS:::::::  ONE STOP PORTAL FOR KIDS NEEDS");
		LoginPage lp=new LoginPage(driver);
		
		lp.clickhomelogin();
		logger.info("Clicked on LOGIN button in Home page");
		Thread.sleep(2500);
		lp.enteremail(username);
		lp.clickpinklogin();
		logger.info("Clicked after email Login button");
		Thread.sleep(2500);
		lp.clickloginwithpwd();
		logger.info("Clicking on LOGINWITHPASSWORD Button");
		Thread.sleep(2500);
		lp.enterpwd(password);
		logger.info("Password entered");
		Thread.sleep(2500);
		lp.clickloginFinal();
		logger.info("Clicked on FINAL LOGIN Button");
	String title=driver.getTitle();
		{
			System.out.println("THE PAGE TITLE IS  "+ title);
			
		}
		
	if(driver.getTitle().contains("One Stop Portal For Kids Needs"))
	{
		Assert.assertTrue(true);
		logger.info("Login test Passed");
	}
	else {
		captureScreen(driver,"LoginTest");
		Assert.assertFalse(false);
		logger.info("Login test Failed");
	}
	Thread.sleep(1000);
//	lp.clicklogout();
//	logger.info("Logged out successfully");
	}
}
	
/*	 public boolean isAlertPresent() //
	 {
	  try {
		  driver.switchTo().alert().accept();;
		  return true;
	  }
	  catch(NoAlertPresentException e)
	  {
		  return false;  */
	

